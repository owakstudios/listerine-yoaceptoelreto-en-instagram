<?php namespace Listerine\Instagram;

use \Carbon\Carbon;
use \Instagram\Instagram;
use \Illuminate\Support\Collection;
use Listerine\Instagram\Contracts\InstagramClient;

class InstagramCollector implements InstagramClient {

	/**
	 * save an instagram API instance
	 */
	private $instagram;

	/**
	 * hastags we're going to look up for
	 * @var [type]
	 */
	private $hashtags;

	/**
	 * minimun count for instagram media to return
	 * @var integer
	 */
	private $min_count = 50;

	/**
	 * exclude these users
	 * @var array
	 */
	private $exclude_users = array('listerineco');

	/**
	 * constructor
	 */
	public function __construct(Instagram $instagram, $hashtags = array())
	{
		$this->instagram = $instagram;
		$this->hashtags = $hashtags[0];

		$this->starts_at = Carbon::createFromDate(2015, 06, 05, "America/Bogota");
		$this->ends_at = Carbon::createFromDate(2015, 07, 05, "America/Bogota");
	}

	//-----------------------------------------------
	/**
	 * retrieves all instagram media that has the specified hashtag
	 * @return [type] [description]
	 */
	public function mediaWithHashtag()
	{
		$media = $this->fetchMediaForHashtag(array('count' => $this->min_count));

		$media = $this->excludeListerinePosts($media);
		$media = $this->filterMediaByDate($media, $this->starts_at, $this->ends_at);

		return $media;
	}

	protected function fetchMediaForHashtag($params)
	{
		$tag      = $this->instagram->getTag($this->hashtags);
		$media    = Collection::make(array());
		$mediaRaw = $tag->getMedia($params);

		$media = $media->merge($mediaRaw->getData());

		if($mediaRaw->getNext()) {
			$params['max_tag_id'] = $mediaRaw->getNext();
			$this->fetchMediaForHashtag($params);
		}

		return $media;
	}

	/**
	 * get the hashtag object
	 * @return [type] [description]
	 */
	public function getHashtag()
	{
		return $this->instagram->getTag($this->hashtags);
	}

	/**
	 * returns true if the user follows listerine
	 * @param  [type] $user [description]
	 * @return [type]       [description]
	 */
	public function userFollowsAccount($user)
	{
		$follows = $this->getFollows($user);

  	$followsListerine = $follows->filter(function($follow)
  	{
  		return $follow == 'listerineco';
  	})->first();

  	return isset($followsListerine);
	}

	/**
	 * get follows
	 * @param  [type] $user [description]
	 * @return [type]       [description]
	 */
	public function getFollows($user)
	{
		$following = $user->getFollows()->getData();
		$follows = Collection::make($following);

		return $follows->map(function($follow)
		{
			return $follow->getUserName();
		});
	}

	/**
	 * gets media with the desired hastag from the user's feed
	 * @param  [type] $user [description]
	 * @return [type]       [description]
	 */
	public function getMediaFromUser($user)
	{
		$media = $user->getMedia()->getData();
		$media = Collection::make($media);
		$hashtag = strtolower($this->hashtags);


		return $media->filter(function($media) use ($hashtag)
		{
			$tags = $media->getTags()->toArray();
			return in_array($hashtag, $tags);
		});
	}

	//---------------------------------------------------

	protected function filterMediaByDate($media, $from, $to)
	{
		return $media->filter(function($media) use ($from, $to)
		{
			$mediaDate = Carbon::createFromTimestamp($media->getCreatedTime());
			return $mediaDate->between($from, $to);
		});
	}

	protected function excludeListerinePosts($media)
	{
		return $media->filter(function($media)
		{
			$user        = $media->getUser()->getUserName();
			$isListerine = strpos($user, 'listerine');
			return $isListerine !== 0;
		});
	}
}