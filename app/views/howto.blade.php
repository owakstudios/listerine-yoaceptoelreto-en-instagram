@extends('layouts.master')

@section('content')
<section style="margin-top: 10px; margin-top: 10px; padding-bottom: 20px; padding-top: 20px;">

	<div class="title" style="padding:3px;">
		<h2>MECÁNICA</h2>
		<p>*Actividad válida del 08/06/15 al 08/07/15 | Aplican condiciones*</p>
	</div>

	<ul class="pasos">

		<li>
			<div class="numero">
				1
			</div>

			<div class="descripcion">
				<p>Síguenos en <img src="{{ asset('public/img/logoInstagram.png') }}"/></p><br><p style="margin-top: -48px;">búscanos como <strong>@ListerineCO</strong></p>
			</div>
		</li>

		<li>
			<div class="numero">
				2
			</div>

			<div class="descripcion" style="margin-top:-9px;">
				<p>Comparte una foto en la que muestres cómo llevas a cabo los retos usando el <strong>HASHTAG #YoAceptoElReto</strong></p>
			</div>
		</li>
	</ul>

	<a href="http://www.listerine.co/reto-21-dias/cuentanos-tu-reto/como-participo" target="_parent"> <div  id="resultados">
		Revisa aquí los retadores en tiempo real
	</div></a>

	<div style="width: 400px; margin: 22px auto; font-weight: lighter; font-size: 9px; color: #666; font-family: 'gotham_bookregular';">
		*Actividad válida del 8 de junio al 8 de julio de 2015.<br>El premio por persona incluye productos LISTERINE®<br> Los likes en las fotos no representan puntos para ganar. Se evaluarán a los ganadores<br> por el tiempo que se tomen en llevar a cabo el reto y publicar su foto en INSTAGRAM.
	</div>
</section>
@stop