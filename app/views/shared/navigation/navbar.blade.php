<?php $active = Route::currentRouteName() ?>
<header>
	<nav>
		<ul>
			<li class="{{ $active == 'pages.how-to-participate' ? 'active' : '' }}">
				<h1 style="margin-top: 23px;"><a href="http://www.listerine.co/reto-21-dias/cuentanos-tu-reto/como-participo" target="_parent">¿Cómo participo?</a></h1>
			</li>
			<li class="{{ $active == 'home' ? 'active' : '' }}">
				<h1 style="margin-top: 23px;"><a href="http://www.listerine.co/reto-21-dias/cuentanos-tu-reto" target="_parent">#YoAceptoElReto</a></h1>
			</li>
			<li class="{{ $active == 'legals.terms-and-conditions' ? 'active' : '' }}">
				<h1 style="margin-top: 14px;"><a href="http://www.listerine.co/reto-21-dias/cuentanos-tu-reto/terminos-y-condiciones" target="_parent">Términos y<br>Condiciones</a></h1>
			</li>
		</ul>
	</nav>
</header>